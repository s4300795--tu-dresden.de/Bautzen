#!/bin/bash

# exit on first error
set -e

# run virtual environment
source myenv/bin/activate

# load taurus modules
module --force purge
module load modenv/scs5
module load Python/3.8.6

# download data
#!/bin/sh
for i in {0..23}; do python cds_api_script.py 2021 9 6 $i; done
for i in {0..23}; do python cds_api_script.py 2021 9 7 $i; done
for i in {0..23}; do python cds_api_script.py 2021 9 8 $i; done
for i in {0..23}; do python cds_api_script.py 2021 9 9 $i; done
for i in {0..23}; do python cds_api_script.py 2021 9 10 $i; done

# deactivate virtual environment
deactivate
